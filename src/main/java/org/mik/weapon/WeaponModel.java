package org.mik.weapon;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
 * 
 */
@DatabaseTable(tableName = "Weapon")
public class WeaponModel extends AbstractWeapon {

	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String TYPE = "type";
	public static final String AMMO = "ammo";
	public static final String DAMAGE = "damage";

	@DatabaseField(columnName = ID, generatedId = true)
	private int id;

	@DatabaseField(columnName = NAME)
	private String name;

	@DatabaseField(columnName = TYPE)
	private String type;

	@DatabaseField(columnName = AMMO)
	private String ammo;

	@DatabaseField(columnName = DAMAGE)
	private int damage;

	public WeaponModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WeaponModel(int id, String name, String type, String ammo, int damage) {
		super(id, name, type, ammo, damage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getAmmo() {
		return ammo;
	}

	@Override
	public void setAmmo(String ammo) {
		this.ammo = ammo;
	}

	@Override
	public int getDamage() {
		return damage;
	}

	@Override
	public void setDamage(int damage) {
		this.damage = damage;
	}

	public boolean isAssaultRifle() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSniperRifle() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSubmachineGun() {
		// TODO Auto-generated method stub
		return false;
	}
}
