package org.mik.weapon;

public interface Weapon {
	public int getId();

	public String getName();

	public String getType();

	public int getDamage();

	public String getAmmo();

	public boolean isAssaultRifle();

	public boolean isSniperRifle();

	public boolean isSubmachineGun();
}
