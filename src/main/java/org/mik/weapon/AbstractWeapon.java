package org.mik.weapon;

public abstract class AbstractWeapon implements Weapon {

	private int id;

	private String name;

	private String type;

	private String ammo;

	private int damage;

	public AbstractWeapon(int id, String name, String type, String ammo, int damage) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.ammo = ammo;
		this.damage = damage;
	}

	public AbstractWeapon() {
		// TODO Auto-generated constructor s {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAmmo() {
		return ammo;
	}

	public void setAmmo(String ammo) {
		this.ammo = ammo;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}
}
