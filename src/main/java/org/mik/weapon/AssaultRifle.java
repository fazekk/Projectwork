package org.mik.weapon;

public class AssaultRifle extends AbstractWeapon{

	public AssaultRifle(int id,String name, String type, String ammo, int damage) {
		
		super(id,name, type, ammo, damage);
	
	}
	
	public boolean isAssaultRifle() {
	
		return true;
	}

	public boolean isSniperRifle() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSubmachineGun() {
		// TODO Auto-generated method stub
		return false;
	}	
}
