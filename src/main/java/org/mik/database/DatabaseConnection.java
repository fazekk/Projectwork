package org.mik.database;

import java.io.IOException;
import java.sql.*;

import org.mik.weapon.WeaponModel;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;


public class DatabaseConnection {


	public static Dao<WeaponModel, String> weaponDao;
	static ConnectionSource connectionSource;

	/*
	 * 
	 */
	@SuppressWarnings("null")
	public static void Connector() throws ClassNotFoundException {
		try {

			String databaseUrl = "jdbc:mysql://fazek.synology.me:3307/Weapons?user=projectwork&password=project123&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			// create a connection source to our database
			 connectionSource = new JdbcConnectionSource(databaseUrl);

			weaponDao = DaoManager.createDao(connectionSource, WeaponModel.class);

		} catch (SQLException e) {
			System.out.println(e);
			// e.printStackTrace();
		}
	}
	
	/*
	 * 
	 */
	public static void Disconnect() throws IOException {
		connectionSource.close();
	}

}
