package org.mik.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

import org.mik.database.DatabaseConnection;

import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;

public class WeaponUI {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private ButtonGroup typeButtonGroup = new ButtonGroup();
	private ButtonGroup ammoButtonGroup = new ButtonGroup();
	public JTable table;
	private static WeaponUiController uiController;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WeaponUI window = new WeaponUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					DatabaseConnection.Connector();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					uiController.LoadTable();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WeaponUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 829, 698);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					uiController.UpdateTable();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnUpdate.setBounds(12, 600, 97, 25);
		frame.getContentPane().add(btnUpdate);

		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					uiController.RemoveRow();
					uiController.LoadTable();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnRemove.setBounds(121, 600, 97, 25);
		frame.getContentPane().add(btnRemove);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 162, 783, 425);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		table.setCellSelectionEnabled(true);
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {}));
		scrollPane.setViewportView(table);
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, null, null, null));

		uiController = new WeaponUiController(table);

		JButton btnRemoveAll = new JButton("Remove All");
		btnRemoveAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					uiController.RemoveAll();
					uiController.LoadTable();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnRemoveAll.setBounds(233, 600, 97, 25);
		frame.getContentPane().add(btnRemoveAll);

		JButton btnFindById = new JButton("Find By Id");
		btnFindById.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					uiController.FindById(textField_2);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnFindById.setBounds(383, 600, 97, 25);
		frame.getContentPane().add(btnFindById);

		textField_2 = new JTextField();
		textField_2.setBounds(487, 600, 26, 25);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);

		JButton btnNewButton = new JButton("Refresh Table");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					uiController.LoadTable();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(639, 600, 156, 25);
		frame.getContentPane().add(btnNewButton);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, null, null, null));
		panel_2.setBounds(12, 13, 783, 145);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(234, 36, 363, 38);
		panel_2.add(panel);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, null, null, null));
		panel.setLayout(null);

		final JRadioButton rdbtnAssaultrifle = new JRadioButton("Assault Rifle");
		rdbtnAssaultrifle.setBounds(8, 9, 99, 25);
		panel.add(rdbtnAssaultrifle);

		JRadioButton rdbtnSubMachineGun = new JRadioButton("Submachine Gun");
		rdbtnSubMachineGun.setBounds(117, 9, 127, 25);
		panel.add(rdbtnSubMachineGun);

		JRadioButton rdbtnSniperRifle = new JRadioButton("Sniper Rifle");
		rdbtnSniperRifle.setBounds(248, 9, 107, 25);
		panel.add(rdbtnSniperRifle);

		rdbtnAssaultrifle.setActionCommand("Assault Rifle");
		rdbtnSubMachineGun.setActionCommand("Submachine Gun");
		rdbtnSniperRifle.setActionCommand("Sniper Rifle");

		typeButtonGroup.add(rdbtnAssaultrifle);
		typeButtonGroup.add(rdbtnSubMachineGun);
		typeButtonGroup.add(rdbtnSniperRifle);

		textField_1 = new JTextField();
		textField_1.setBounds(492, 110, 105, 22);
		panel_2.add(textField_1);
		textField_1.setColumns(10);

		JLabel lblDamage = new JLabel("Damage:");
		lblDamage.setBounds(435, 113, 56, 16);
		panel_2.add(lblDamage);

		JLabel lblType = new JLabel("Type:");
		lblType.setBounds(190, 55, 40, 16);
		panel_2.add(lblType);

		textField = new JTextField();
		textField.setBounds(62, 52, 116, 22);
		panel_2.add(textField);
		textField.setColumns(10);

		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(6, 52, 44, 22);
		panel_2.add(lblName);

		JLabel lblAmmo = new JLabel("Ammo:");
		lblAmmo.setBounds(6, 113, 44, 16);
		panel_2.add(lblAmmo);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(60, 94, 363, 38);
		panel_2.add(panel_1);
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, null, null, null));
		panel_1.setLayout(null);

		final JRadioButton rdbtnmm = new JRadioButton("7.62mm");
		rdbtnmm.setBounds(8, 9, 99, 25);
		panel_1.add(rdbtnmm);

		JRadioButton rdbtnmm_1 = new JRadioButton("5.56mm");
		rdbtnmm_1.setBounds(111, 9, 99, 25);
		panel_1.add(rdbtnmm_1);

		JRadioButton rdbtnmm_2 = new JRadioButton("9mm");
		rdbtnmm_2.setBounds(214, 9, 75, 25);
		panel_1.add(rdbtnmm_2);

		JRadioButton radioButton = new JRadioButton(".45");
		radioButton.setBounds(293, 9, 47, 25);
		panel_1.add(radioButton);

		rdbtnmm.setActionCommand("7.62mm");
		rdbtnmm_1.setActionCommand("5.56mm");
		rdbtnmm_2.setActionCommand("9mm");
		radioButton.setActionCommand(".45");
		ammoButtonGroup.add(rdbtnmm);
		ammoButtonGroup.add(rdbtnmm_1);
		ammoButtonGroup.add(rdbtnmm_2);
		ammoButtonGroup.add(radioButton);

		JButton btnAddNewData = new JButton("Add");
		btnAddNewData.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAddNewData.setBounds(609, 36, 163, 96);
		panel_2.add(btnAddNewData);

		JLabel lblAddNewData = new JLabel("Add new data:");
		lblAddNewData.setBounds(6, 0, 105, 29);
		panel_2.add(lblAddNewData);
		btnAddNewData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					uiController.AddNewData(textField, typeButtonGroup.getSelection().getActionCommand(),
							ammoButtonGroup.getSelection().getActionCommand(), textField_1);
					uiController.LoadTable();

				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        if (JOptionPane.showConfirmDialog(frame, 
		            "Are you sure to close this window?", "Really Closing?", 
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		        	try {
						DatabaseConnection.Disconnect();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		            System.exit(0);
		        }
		    }
		});
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

	}
}
