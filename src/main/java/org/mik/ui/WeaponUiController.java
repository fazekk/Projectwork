package org.mik.ui;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.JTextField;

import org.mik.database.DatabaseConnection;
import org.mik.weapon.WeaponModel;


public class WeaponUiController {

	public WeaponUiController(JTable table) {
		super();
		this.table = table;
	}

	private JTable table;
	public static int selectedRowIndex;
/*
 * 
 */
	public void LoadTable() throws SQLException {

		List<WeaponModel> weapons = DatabaseConnection.weaponDao.queryForAll();
		WeaponTableModel model = new WeaponTableModel(weapons);


		model.fireTableDataChanged();
		table.setModel(model);
	}
/**
 * 
 * @param nameText
 * @param typeRadioButton
 * @param ammoRadioButton
 * @param damageText
 * @throws SQLException
 */
	public void AddNewData(JTextField nameText,String typeRadioButton,String ammoRadioButton,JTextField damageText) throws SQLException {
		WeaponModel abstractWeapon = new WeaponModel();
		abstractWeapon.setName(nameText.getText());
		abstractWeapon.setType(typeRadioButton);
		abstractWeapon.setAmmo(ammoRadioButton);
		abstractWeapon.setDamage(Integer.parseInt(damageText.getText()));
		DatabaseConnection.weaponDao.create(abstractWeapon);
	}

	/*
	 * 
	 */
	public void UpdateTable() throws SQLException {
		for (WeaponModel abstractWeapon2 : WeaponTableModel.weapons) {
			System.out.println(abstractWeapon2);
			DatabaseConnection.weaponDao.update(abstractWeapon2);
		}
	}
/*
 * 
 */
	public void RemoveRow() throws SQLException {
		selectedRowIndex = table.getSelectedRow();
		WeaponModel abstractWeapon = WeaponTableModel.weapons.get(selectedRowIndex);
		DatabaseConnection.weaponDao.delete(abstractWeapon);
	}
	
	/*
	 * 
	 */
	public void RemoveAll() throws SQLException {
		for (WeaponModel abstractWeapon : WeaponTableModel.weapons) {
			DatabaseConnection.weaponDao.delete(abstractWeapon);
		}
	}
	
	/*
	 * 
	 */
	public void FindById(JTextField text) throws SQLException {
	List<WeaponModel>abstractWeapon =new ArrayList<WeaponModel>();
	abstractWeapon.add(DatabaseConnection.weaponDao.queryForId(text.getText()));
		WeaponTableModel model = new WeaponTableModel(abstractWeapon);
		model.fireTableDataChanged();
		table.setModel(model);
	}
}
