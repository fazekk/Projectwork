package org.mik.ui;

import java.util.ArrayList;
import java.util.List;


import javax.swing.table.AbstractTableModel;

import org.mik.weapon.WeaponModel;

public class WeaponTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3281976840746421255L;
	static List<WeaponModel> weapons;
	private String[] columns;

	/*
	 * 
	 */
	public WeaponTableModel(List<WeaponModel> weaponList) {
		super();
		this.weapons = weaponList;
		columns = new String[] { "id", "name", "type", "ammo", "damage" };
	}

	// The object to render in a cell
	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int row, int col) {
		WeaponModel abstractWeapon = weapons.get(row);
		switch (col) {
		case 0:
			return abstractWeapon.getId();
		case 1:
			return abstractWeapon.getName();
		case 2:
			return abstractWeapon.getType();
		case 3:
			return abstractWeapon.getAmmo();
		case 4:
			return abstractWeapon.getDamage();
		default:
			return null;
		}
	}

	// Optional, the name of your column
	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int col) {
		return columns[col];
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		// TODO Auto-generated method stub
		return weapons.size();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columns.length;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int row, int col) {
		return col == 0 ? false : true;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
	 */
	public void setValueAt(Object value, int row, int col) {
		WeaponModel abstractWeapon = weapons.get(row);
		switch (col) {
		case 0:
			break;
		case 1:
			abstractWeapon.setName((String) value);
			break;
		case 2:
			abstractWeapon.setType((String) value);
			break;
		case 3:
			abstractWeapon.setAmmo((String) value);
			break;
		case 4:
			abstractWeapon.setDamage((Integer) value);
			break;
		default:

		} // save edits some where
		fireTableCellUpdated(row, col);
		List<WeaponModel> weapons = new ArrayList<WeaponModel>();
		weapons.add(abstractWeapon);
	}
}
