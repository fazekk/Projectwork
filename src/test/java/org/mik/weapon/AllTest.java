package org.mik.weapon;

import static org.junit.Assert.*;

import org.junit.Test;



import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class AllTest {

	@Test
	public void Alltest() {
		classTest();
		objectTest();
	

	}

	public void classTest() {
		try {

			
			// WEAPON

			assertTrue(Weapon.class.isInterface());

			Method m = Weapon.class.getMethod("getId"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(int.class, m.getReturnType());

			m = Weapon.class.getMethod("getName"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(String.class, m.getReturnType());

			m = Weapon.class.getMethod("getType"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(String.class, m.getReturnType());

			m = Weapon.class.getMethod("getAmmo"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(String.class, m.getReturnType());

			m = Weapon.class.getMethod("getDamage"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(int.class, m.getReturnType());

			m = Weapon.class.getMethod("isAssaultRifle"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), boolean.class);

			m = Weapon.class.getMethod("isSniperRifle"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), boolean.class);

			m = Weapon.class.getMethod("isSubmachineGun"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), boolean.class);

			// AbstractWEAPON

			assertFalse(AbstractWeapon.class.isInterface());

			assertTrue(Modifier.isAbstract(AbstractWeapon.class.getModifiers()));

			assertTrue(Arrays.asList(AbstractWeapon.class.getInterfaces()).contains(Weapon.class));

			Constructor<?> c = AbstractWeapon.class.getConstructor(int.class, String.class, String.class, String.class,
					int.class);
			assertNotNull(c);

			m = AbstractWeapon.class.getMethod("getId"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), int.class);

			m = AbstractWeapon.class.getMethod("setId", int.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			m = AbstractWeapon.class.getMethod("getName"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), String.class);

			m = AbstractWeapon.class.getMethod("setName", String.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			m = AbstractWeapon.class.getMethod("getType"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), String.class);

			m = AbstractWeapon.class.getMethod("setType", String.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			m = AbstractWeapon.class.getMethod("getAmmo"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), String.class);

			m = AbstractWeapon.class.getMethod("setAmmo", String.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			m = AbstractWeapon.class.getMethod("getDamage"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), int.class);

			m = AbstractWeapon.class.getMethod("setDamage", int.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			assertFalse(AssaultRifle.class.isInterface());

			assertFalse(Modifier.isAbstract(AssaultRifle.class.getModifiers()));

			assertEquals(AssaultRifle.class.getSuperclass(), AbstractWeapon.class);

			c = AssaultRifle.class.getConstructor(int.class, String.class, String.class, String.class, int.class);
			assertNotNull(c);

			assertFalse(SniperRifle.class.isInterface());

			assertFalse(Modifier.isAbstract(SniperRifle.class.getModifiers()));

			assertEquals(SniperRifle.class.getSuperclass(), AbstractWeapon.class);

			c = SniperRifle.class.getConstructor(int.class, String.class, String.class, String.class, int.class);
			assertNotNull(c);

			assertFalse(SubmachineGun.class.isInterface());

			assertFalse(Modifier.isAbstract(SubmachineGun.class.getModifiers()));

			assertEquals(SubmachineGun.class.getSuperclass(), AbstractWeapon.class);

			c = SubmachineGun.class.getConstructor(int.class, String.class, String.class, String.class, int.class);
			assertNotNull(c);

			// WEAPONMODEL

			assertFalse(WeaponModel.class.isInterface());

			assertFalse(Modifier.isAbstract(WeaponModel.class.getModifiers()));

			assertEquals(WeaponModel.class.getSuperclass(), AbstractWeapon.class);

			Constructor<?> a = WeaponModel.class.getConstructor();
			assertNotNull(a);

			m = WeaponModel.class.getMethod("getId"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), int.class);

			m = WeaponModel.class.getMethod("setId", int.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			m = WeaponModel.class.getMethod("getName"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), String.class);

			m = WeaponModel.class.getMethod("setName", String.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			m = WeaponModel.class.getMethod("getType"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), String.class);

			m = WeaponModel.class.getMethod("setType", String.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			m = WeaponModel.class.getMethod("getAmmo"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), String.class);

			m = WeaponModel.class.getMethod("setAmmo", String.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

			m = WeaponModel.class.getMethod("getDamage"); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), int.class);

			m = WeaponModel.class.getMethod("setDamage", int.class); //$NON-NLS-1$
			assertNotNull(m);

			assertEquals(m.getReturnType(), void.class);

		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	private int id;

	private String name;

	private String type;

	private String ammo;

	private int damage;
	
	private void objectTest() {

		
		WeaponModel w1 = new WeaponModel(id, name, type, ammo, damage);
		assertNotNull(w1);
		assertEquals(id, w1.getId());
		assertEquals(name, w1.getName());
		assertEquals(type, w1.getType());
		assertEquals(ammo, w1.getAmmo());
		assertEquals(damage, w1.getDamage());
		
		WeaponModel w2 = new WeaponModel(id, name, type, ammo, damage);
		assertNotNull(w2);
		assertEquals(id, w2.getId());
		assertEquals(name, w2.getName());
		assertEquals(type, w2.getType());
		assertEquals(ammo, w2.getAmmo());
		assertEquals(damage, w2.getDamage());
		
		w1.setId(12);
		assertEquals(12, w1.getId());
		
		w1.setName("AKM");
		assertEquals("AKM", w1.getName());
		
		w1.setType("AssaultRifle");
		assertEquals("AssaultRifle", w1.getType());
		
		w1.setAmmo("7.62mm");
		assertEquals("7.62mm", w1.getAmmo());
		
		w1.setDamage(45);
		assertEquals(45, w1.getDamage());
		
		w2.setId(12);
		assertEquals(12, w2.getId());
		
		w2.setName("M416");
		assertEquals("M416", w2.getName());
		
		w2.setType("AssaultRifle");
		assertEquals("AssaultRifle", w2.getType());
		
		w2.setAmmo("5.56mm");
		assertEquals("5.56mm", w2.getAmmo());
		
		w2.setDamage(60);
		assertEquals(60, w2.getDamage());

		
		assertFalse(w1 == w2);
	}

	
		
}